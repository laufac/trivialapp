/*En base a la api Open Trivia
La idea es hacer un juego en el que el usuario introduzca en un input las caracteristicas del Trivial y que al darle al 
'Start Game' le salgan las preguntas de la api para que pueda comenzar el juego. Una vez las responda todas, 
le mostraremos al usuario el resultado.

Ten en cuenta que hay dos tipos de preguntas. Aquellas con 3 respuestas erroneas y una correcta y aquellas con respuesta 
verdadero / falso.*/

const correct_answers = [];
let count_passed = 0;
let input$$ = 10;

let tablero$$ = document.createElement('div');

var decodeHTML = function (html) {

    var txt = document.createElement('textarea');
  
    txt.innerHTML = html;
  
    return txt.value;
  
  };

let handleChange = function(event , btn){
    event.target.previousElementSibling.remove();
    event.target.remove();
}

const startGame = async () => {
    tablero$$.remove();
    input$$ = document.querySelector('[data-function="question-number"]').value;

    const resParsedtoJSON = await fetch('https://opentdb.com/api.php?amount='+ input$$ + '&type=multiple').then(res => res.json());
    console.log(resParsedtoJSON.results);
    createQuestions(resParsedtoJSON.results)
}

const button$$ = document.querySelector('[data-function="start-game"]');
button$$.addEventListener('click', startGame);


function createQuestions(questions){
    const btnCheck$$ = document.createElement('button');
    btnCheck$$.textContent = "Check!";
    btnCheck$$.addEventListener('click', function(e) {
        e.preventDefault();
        checkFinalAnswer();
    });


    for (const question of questions) { //res.results porque res es un objeto
        
        const div$$ = document.createElement('div');
        const h3_question$$ = document.createElement('h3');
        h3_question$$.textContent = question.question;
        div$$.appendChild(h3_question$$);
        correct_answers.push(question.correct_answer);

        
        //Me hago un array para almacenar las respuestas y luego poder mostrarlas desordenadas
        const answers = [question.correct_answer + '1'];
        const rand_array = [];

        for(let i=0; i<3; i++){
            answers.push(question.incorrect_answers[i]); 
        }

        for(let i=0; i<answers.length; i++){
            let rand = Math.round(Math.random()*3);
            while(rand_array.includes(rand)){
                rand = Math.round(Math.random()*3);
            }
            rand_array.push(rand);
            const p_resp$$ = document.createElement('p');
            if(answers[rand].charAt(answers[rand].length-1) === '1'){
                p_resp$$.textContent = answers[rand].slice(0, p_resp$$.textContent.length-1);
                p_resp$$.classList.add('correct-answer');
            }else{
                p_resp$$.textContent = answers[rand];
            }
            div$$.appendChild(p_resp$$);

            //Escuchador
            p_resp$$.addEventListener('click', function(e) {
                e.preventDefault();
                checkAnswer(p_resp$$, question.correct_answer, answers.length);
            });
        }
        tablero$$.appendChild(div$$);
    }
    tablero$$.appendChild(btnCheck$$);
    document.body.appendChild(tablero$$);
}


function checkAnswer(answer, correct_answer, len){   
    const div$$ = answer.parentNode;
    const nodos$$ = div$$.childNodes;

    for(let i=0; i<len+1; i++){
        if(nodos$$[i].classList == 'selected-answer'){
            nodos$$[i].classList.remove('selected-answer');
            nodos$$[i].removeAttribute('style');
            
        }
    }
    
    answer.style.color = "magenta";
    if(answer.textContent===correct_answer){
        answer.classList.add('selected-answer');
        count_passed++;
    }else{
        answer.classList.add('selected-answer');
    }
}

function checkFinalAnswer (){
    const pSSelected$$ = document.querySelectorAll('.selected-answer');
    const pSGood$$ = document.querySelectorAll('.correct-answer');

    if(pSSelected$$.length != input$$){
        alert ("You must answer all questions before checking your results!");
    }else{
        

        for (const pSelected$$ of pSSelected$$) {
            pSelected$$.style.color = 'red';
        }
        
        for (const pGood$$ of pSGood$$) {
            pGood$$.style.color = 'green';
        }

        const pResult$$ = document.createElement('p');
        pResult$$.style.color = "blue";
        pResult$$.textContent = "You have guessed " + count_passed + " answers!";
        tablero$$.appendChild(pResult$$);     
    }
}